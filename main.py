import discord
from decouple import config

#Init Code
TOKEN = config('TOKEN')
client = discord.Client()
message_count = 0

@client.event
async def on_ready():
  print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
  #Track Total Messages Received
  message_count += 1

  #Does Message Have The Prefix?
  if message.content.startswith('!'):
    response = ""

    #Determine Command
    message_list = message.content.split()
    command      = message_list[0]

    #Respond with help message
    if command == "!help":
      response = """```
      !help = prints this help message
      !     = Deletes a message 5 minutes after it is posted.
      !!    = Deletes a message 1 minute after it is posted.
      !!!   = Deletes a message 30 seconds after it is posted.
      !*    = Deletes a message 5 seconds after it is posted.
      ```"""
    #Return Total Message Count
    elif command == "!_mc"
      respone = "Current Message Count: " + message_count

    #Delete message 5 minutes after it is posted.
    elif command == "!":
      await asyncio.sleep(300)
      await message.delete()

    #Delete message 1 minute after it is posted.
    elif command == "!!":
      await asyncio.sleep(60)
      await message.delete()

    #Delete message 30 seconds after it is posted.
    elif command == "!!!":
      await asyncio.sleep(30)
      await message.delete()

    #Delete message 5 seconds after it is posted.
    elif command == "!*":
      await asyncio.sleep(5)
      await message.delete()

    #Unrecognized command
    else:
      response = "Unrecognized Command"

    if response:
      await message.channel.send(response)

client.run(TOKEN)
