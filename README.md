# TimeSensitiveMessage

For blog writeup on this bot: https://voltii.xyz/builds/timesensitive/

This bot will delete discord message marked with a "!" prefix. 

```
!     = Deletes a message 5 minutes after it is posted.
!!    = Deletes a message 1 minute after it is posted.
!!!   = Deletes a message 30 seconds after it is posted.
!*    = Deletes a message 5 seconds after it is posted.
```
